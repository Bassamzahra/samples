<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('Location');
            $table->datetime('Starts');
            $table->datetime('Ends');
            $table->string('Image');
            $table->string('Description');
            $table->string('OrganiserName');
            $table->string('OrganiserDescription');
            $table->integer('EventCategoryId');
            $table->integer('EventTopicId');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
