<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'name' ,
        'Location',
        'Starts' ,
        'Ends' ,
        'Image' ,
        'Description' ,
        'OrganiserName' ,
        'OrganiserDescription' ,
        'EventCategoryId' ,
        'EventTopicId'
        
            
    ]; 

}
