<?php 


namespace App\Http\Controllers;

use App\EventTopic;
use Illuminate\Http\Request;

class EventTopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

  

    public function index()
    {
        $eventtopic = EventTopic::all();
        return view('eventtopic.index', compact('eventtopic'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eventtopic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        EventTopic::create($request->all());
        return redirect()->route('eventtopic.index')

            ->with('success', 'EventTopic created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventTopic  $eventtopic
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $eventtopic = EventTopic::find($id);
        return view('eventtopic.readingone',array('eventtopic' => $eventtopic));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventTopic  $eventtopic
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventTopic  $eventtopic
     * @return \Illuminate\Http\Response
     */
    public function edit(EventTopic $eventtopic)
    {
        return view('eventtopic.updateone',compact('eventtopic'));
    }
    public function update(Request $request, EventTopic $eventtopic)
    {

        $request->validate([
            'name' => 'required' ,
        
        ]);

        $eventtopic->update($request->all());

        return redirect()->route('eventtopic.index')
            ->with('success','eventtopic updated successfully');
    }
   
  
     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
  
   
    public function destroy(EventTopic $eventtopic)

    {
          
          $eventtopic->delete();
       
        return redirect()->route('eventtopic.index')
         ->with('success' ,'Successfully deleted the EventTopic!');
    }






    //return de alle eventtopics om binnen de event creat te gebruiken
    

    public static function readall()
    {
      return  $event = EventTopic::all();
       
    }
}
   
