<?php 


namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event = Event::all();
        return view('event.index', compact('event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $eventcategory = EventCategoryController::readall();
        $eventtopic = EventTopicController::readall();
        return view('event.create' ,compact('eventcategory'),compact('eventtopic') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'Location'=> 'required',
            'Starts' => 'required',
            'Ends' => 'required',
            'Image'=> 'required' ,
            'Description'=> 'required' ,
            'OrganiserName'=> 'required' ,
            'OrganiserDescription'=> 'required' ,
            'EventCategoryId'=> 'required' ,
            'EventTopicId'=> 'required' ,
        ]);
        Event::create($request->all());
        return redirect()->route('event.index')

            ->with('success', 'Event created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        return view('event.readingone',array('event' => $event));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event $event
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        $eventcategory = EventCategoryController::readall();
        $eventtopic = EventTopicController::readall();
        return view('event.updateone',compact('event') ,compact('eventcategory'),compact('eventtopic') );
        

    }
    public function update(Request $request, Event $event)
    {

        $request->validate([
            'name' => 'required' ,
            'Location'=> 'required',
            'Starts' => 'required',
            'Ends' => 'required',
            'Image'=> 'required' ,
            'Description'=> 'required' ,
            'OrganiserName'=> 'required' ,
            'OrganiserDescription'=> 'required' ,
            'EventCategoryId'=> 'required' ,
            'EventTopicId'=> 'required' ,
        
        ]);

        $event->update($request->all());

        return redirect()->route('event.index')
            ->with('success','event updated successfully');
    }
   
  
     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
  
   
    public function destroy(Event $event)

    {
          
          $event->delete();
       
        return redirect()->route('event.index')
         ->with('success' ,'Successfully deleted the Event!');
    }
}
   
