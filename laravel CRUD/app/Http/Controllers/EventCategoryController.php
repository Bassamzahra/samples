<?php 


namespace App\Http\Controllers;

use App\EventCategory;
use Illuminate\Http\Request;

class EventCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     

    public function index()
    {
        $eventcategory = EventCategory::all();
        return view('eventcategory.index', compact('eventcategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('eventcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);
        EventCategory::create($request->all());
        return redirect()->route('eventcategory.index')

            ->with('success', 'EventCategory created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventCategory  $eventcategory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $eventcategory = EventCategory::find($id);
        return view('eventcategory.readingone',array('eventcategory' => $eventcategory));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventCategory  $eventcategory
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventCategory  $eventcategory
     * @return \Illuminate\Http\Response
     */
    public function edit(EventCategory $eventcategory)
    {
        return view('eventcategory.updateone',compact('eventcategory'));
    }
    public function update(Request $request, EventCategory $eventcategory)
    {

        $request->validate([
            'name' => 'required' ,
        
        ]);

        $eventcategory->update($request->all());

        return redirect()->route('eventcategory.index')
            ->with('success','eventcategory updated successfully');
    }
   
  
     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
  
   
    public function destroy(EventCategory $eventcategory)

    {
          
          $eventcategory->delete();
       
        return redirect()->route('eventcategory.index')
         ->with('success' ,'Successfully deleted the EventCategory!');
    }



// die function om  return alle eventcategorys voor de event creat 
    public static function readall()
    {
      return  $eventcategory = EventCategory::all();
       
    }



}
   
