@extends('layout')
@section('content')  

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New event</h2>
       </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('event.index') }}"> Back</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div> 
@endif

<form action="{{ route('event.store') }}" method="POST">
    @csrf   
    
     <div class="row">   
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group"> 
            <div> 
                <strong>Name:</strong>
                <input type="text" name="name" class="form-control" placeholder="Name"> 
                </div>
                <div>
                <strong>Location:</strong>
                <input type="text" name="Location" class="form-control" placeholder="Location">
                </div>
                <div>
                <strong>Starts:</strong>
                <input type="datetime" name="Starts" class="form-control" placeholder="Starts">
                </div>
                <div>
                <strong>Ends:</strong>
                <input type="datetime" name="Ends" class="form-control" placeholder="Ends">
                </div>
                <div>
                <strong>Image:</strong>
                <input type="text" name="Image" class="form-control" placeholder="Image">
                </div>
                <div>
                <strong>Description:</strong>
                <input type="text" name="Description" class="form-control" placeholder="Description">
                </div>
                <div>
                <strong>OrganiserName:</strong>
                <input type="text" name="OrganiserName" class="form-control" placeholder="OrganiserName">
                </div>
                <div>
                <strong>OrganiserDescription:</strong>
                <input type="text" name="OrganiserDescription" class="form-control" placeholder="OrganiserDescription">
                </div>
                <div>
                <label for="EventCategoryId">Event categorie</label>
                <select required id="EventCategoryId" name="EventCategoryId" >
                    <!-- option elementen --> 
                @foreach ($eventcategory as $eventcategory) 
            <option value= "{{ $eventcategory->id }}" >  {{ $eventcategory->name }} </option>
            @endforeach
                </select>                
                
               
                </div>  
                <div>
                <label for="EventTopicId">EventTopic Id</label>
                <select required id="EventTopicId" name="EventTopicId" >
                    <!-- option elementen --> 
                @foreach ($eventtopic as $eventtopic) 
            <option value= "{{ $eventtopic->id }}" >  {{ $eventtopic->name }} </option>
            @endforeach
                </select>                
                
               
                </div>
        </div> 
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection