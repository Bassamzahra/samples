
@extends('layout')
@section('content') 

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Reading: {{ $event->name }}</h2> 
       </div> 
       <br>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('event.index') }}"> Back</a>
        </div>
    </div>
</div> 

<table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Location</th>
            <th>Starts</th>
            <th>Ends</th>
            <th>Image</th>
            <th>Description</th>
            <th>OrganiserName</th>
            <th>OrganiserDescription</th>
            <th>EventCategoryId</th>
            <th>EventTopicId</th>
           
        </tr> 

        <td>{{ $event->id }}</td>
            <td>{{ $event->name }}</td> 
            <td>{{ $event->Location }}</td>
            <td>{{ $event->Starts }}</td>
            <td>{{ $event->Ends }}</td>
            <td>{{ $event->Image }}</td>
            <td>{{ $event->Description }}</td>
            <td>{{ $event->OrganiserName }}</td>
            <td>{{ $event->OrganiserDescription }}</td>
            <td>{{ $event->EventCategoryId }}</td>
            <td>{{ $event->EventTopicId }}</td>
        
    </div>
    </table>
    @endsection