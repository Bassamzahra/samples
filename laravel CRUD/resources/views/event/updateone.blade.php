@extends('layout')
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Event</h2>
       </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('event.index') }}"> Back</a>
        </div>
    </div>
</div> 

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('event.update' , $event->id) }}" method="POST">
    @csrf
    @method('PUT')
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
            
            <div>
                <strong>Name:</strong>
               
                <input type="text" name="name" class="form-control"  value="{{ $event->name }}">
            </div>
            <div>
                <strong>Location:</strong>
                <input type="text" name="Location" class="form-control" value="{{ $event->Location }}">
                </div>
                <div>
                <strong>Starts:</strong>
                <input type="datetime" name="Starts" class="form-control" value="{{ $event->Starts }}">
                </div>
                <div>
                <strong>Ends:</strong>
                <input type="datetime" name="Ends" class="form-control" value="{{ $event->Ends }}">
                </div>
                <div>
                <strong>Image:</strong>
                <input type="text" name="Image" class="form-control" value="{{ $event->Image }}">
                </div>
                <div>
                <strong>Description:</strong>
                <input type="text" name="Description" class="form-control" value="{{ $event->Description }}">
                </div>
                <div>
                <strong>OrganiserName:</strong>
                <input type="text" name="OrganiserName" class="form-control" value="{{ $event->OrganiserName }}">
                </div>
                <div>
                <strong>OrganiserDescription:</strong>
                <input type="text" name="OrganiserDescription" class="form-control" value="{{ $event->OrganiserDescription }}">

                </div>
                <div>
                <strong>EventCategory:</strong>
                <input type="text" name="EventCategoryId" class="form-control" value="{{ $event->EventCategoryId }}" readonly>
                
                </div>
                <div>
                <strong>EventTopicId:</strong>
                <input type="text" name="EventTopicId" class="form-control" value="{{ $event->EventTopicId }}" readonly >
                
                </div>
              
                </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection