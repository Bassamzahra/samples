
@extends('layout')
@section('content') 

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Reading: {{ $eventtopic->name }}</h2> 
       </div> 
       <br>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('eventtopic.index') }}"> Back</a>
        </div>
    </div>
</div> 

<table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Name</th>
           
        </tr> 

        <td>{{ $eventtopic->id }}</td>
            <td>{{ $eventtopic->name }}</td> 
        
    </div>
    </table>
    @endsection