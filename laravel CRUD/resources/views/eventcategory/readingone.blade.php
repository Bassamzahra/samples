
@extends('layout')
@section('content') 

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Reading: {{ $eventcategory->name }}</h2> 
       </div> 
       <br>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('eventcategory.index') }}"> Back</a>
        </div>
    </div>
</div> 

<table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Name</th>
           
        </tr> 

        <td>{{ $eventcategory->id }}</td>
            <td>{{ $eventcategory->name }}</td> 
        
    </div>
    </table>
    @endsection