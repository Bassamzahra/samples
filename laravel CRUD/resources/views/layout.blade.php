<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>FricFrac </title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
        <!-- Styles -->
        <style>   
* { 
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
}

body, html {
    height: 100%;
    width: 100%;
}
.page {
    display: flex;
    flex-direction: column;
    height: 100%;
}
.page-header {
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 100%;
    min-height: 75px;
    padding: 0 1em 0 1em;
    background-color: #5F9EA0 ;
    color: rgb(250, 240, 230);
    
}
.index {
    flex-grow: 1;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-around;
    align-content: flex-start;
    width: 100%;
}

    
.page-footer {
    width: 100%;
    min-height: 10%;
    color: rgb(250, 240, 230);
    background-color: rgb(65, 74, 76);
    padding: 0.5em 0 0 1em;
}   
.button {
    background-color : #5F9EA0 ; 
    color : white ;
    padding: 1em 1.5em;
    font-size: 1.2em;

}
        </style> 


    </head> 
    <header class="page-header">
        <nav class="control-panel">
            <a  class= "button" href="/" class="tile">  Home             
            </a>
        </nav>
        <h1 class="banner">Fric-frac Laravel</h1>
    </header>
    <body class="page" >
    <article class="index">
        <div class="container"> 
            <br> <br>
    @yield('content') 
    
       </div>
       </article> 
</body>

    <footer class="page-footer">
        <p>&copy ModernWays 2020</p>
        <p>Opdracht Programmeren 4</p>
    </footer>
</html>