import React, {Component} from 'react';
import Map from './Map';
import Header from './Header';
import Footer from './Footer';
import Comment from './Comment';
import Like from './Like';
class Detail extends Component {

    render() {
   
         let url = `/images/big/${this.props.data.image}`;
        let alt = `Foto van ${this.props.data.name}`;
        return (
            
            <main>
                <Header/>
                
                  
                   <h2>Information over: </h2>
                   <h5>{this.props.data.name} Hotels </h5>
                   
              <table>  
              <tr>
                  
     <th>Hotel Name</th>
    <th>Description</th>
    <th>Type </th>
    <th>Country</th>
   < th>city</th>
  </tr> 
  <tr>
  <td>{this.props.data.name}</td>
    <td>{this.props.data.comment}</td>
    <td>{this.props.data.type}</td>
    <td>{this.props.data.country}</td>
    <td>{this.props.data.city}</td>
   
  </tr>   

                </table> 

                <div className="bigImg"><img id="idimg" src={url} alt={alt}/>
                 <Like data={this.props.data} />
                 <Comment data={this.props.data} />
                </div>
                <div id="mapid">{<Map data={this.props.data} />}</div>

             </main>
        );
        
    }

}

export default Detail; 