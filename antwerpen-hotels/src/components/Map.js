import React ,{Component} from 'react';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import styled from 'styled-components';

const Wrapper = styled.div`
width:${props => props.width};
height:${props => props.height}`;
export default class Map extends Component{
    latitude = this.props.data.latitude;
    longitude = this.props.data.longitude;
    componentDidMount(){
     const map = L.map('map').setView([this.longitude,this.latitude], 13)
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
            detectRetina:true,
            maxZoom:20,
            maxNativeZoom:17,
        }).addTo(map);
        var circle = L.circle([this.longitude, this.latitude], {
            color: 'blue',
            fillColor: '#f03',
            fillOpacity: 0.1,
            radius: 10
        }).addTo(map); 
        
        
    }
    render(){
        return (
            <Wrapper width = '600px' height='400px' id="map"/>
            );
    }
}