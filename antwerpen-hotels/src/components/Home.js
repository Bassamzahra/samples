import React,{Component} from 'react';
import data from '../data/hotels.json';
import Header from './Header';
import Footer from './Footer';

export default class List extends Component{
    list = data.hotels;
    
    
    row = this.list.map(item => {
        let url = `/images/small/${item.image}`;
        let alt = `Foto van ${item.name}`;
        return(
            
            <div className="responsive" >
                <div className="gallery">
                <div className="grid">
                  <button className="btngotoD" onClick={() => this.props.action('Detail',item)}>
                   <img src = {url} alt ={alt}  />
                 </button>
                 </div>
                 </div>
                  <div className="desc">{item.name}</div>
            </div>
            )
    })
    render(){
        return(
           
            <main>
                 <Header/>
               
                   
           {this.row}
           <Footer/>
            </main>
            
            );
    }
}