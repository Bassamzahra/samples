<?php

namespace Fricfrac\Controllers;

class EventTopicController extends \ThreepennyMVC\Controller
{
    public function index()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }

    public function insertingOne()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    } 

    public function createOne()
    {
        $model = array(
            'tableName' => 'EventTopic',
            'error' => 'Geen'
        );
        $EventTopic = array(
            "Name" => $_POST['Name']
        );

        if (\AnOrmApart\Dal::create('EventTopic', $EventTopic, 'Name')) {
            $model['message'] = "Rij toegevoegd! {$EventTopic['Name']} is toegevoegd aan EventTopic";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$EventTopic['Name']} niet toevoegen aan EventTopic";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        return $this->view($model, 'Views/EventTopic/Index.php');
    }

    public function readingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('EventTopic', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    }
    public function deleteOne($Id) 
    { 
        $EventTopicOne= \AnOrmApart\Dal::readOne('EventTopic', $Id);

    
        $model['message'] = \AnOrmApart\Dal::getMessage();


        if (\AnOrmApart\Dal::delete('EventTopic',$Id)) {
            $model['message'] = "EventTopic met de naam {$EventTopicOne['Name']} is verwijderd aan EventTopic Table";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan niet {$EventTopicOne['Name']} verwijderd zijn van EventTopic table";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        return $this->view($model, 'Views/EventTopic/DeleteOne.php');

        
    } 

    public function UpdatingOne($Id) 
    { 
        $model['row'] = \AnOrmApart\Dal::readOne('EventTopic', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        
        return $this->view($model, 'Views/EventTopic/UpdatingOne.php');

        
    }
    public function UpdateOne()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        $model['message'] = \AnOrmApart\Dal::getMessage();

        $EventTopic = array(
           "Id"=> $_POST['Id'] ,
            "Name" => $_POST['Name'] 

        );

        if (\AnOrmApart\Dal::update('EventTopic', $EventTopic, 'Name')) {
            $model['message'] = "Update is geworden voor {$EventTopic['Name']} ";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$EventTopic['Name']} niet update ";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('EventTopic');
        return $this->view($model, 'Views/EventTopic/Index.php');
    }


}

