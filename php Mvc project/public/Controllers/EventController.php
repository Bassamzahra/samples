<?php

/**
 * Created by ModernWays
 * User: Jef Inghelbrecht
 * Date: 3/04/2020
 * Time: 13:32
 */

namespace Fricfrac\Controllers;

class EventController extends \ThreepennyMVC\Controller
{
    public function index()
    { 
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
       
    }
    public function insertingOne()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    } 

    public function createOne()
    {
        $model = array(
            'tableName' => 'Event',
            'error' => 'Geen'
        );
        $Event = array( 

            "Name" => $_POST['Name'] ,
            "Location"=> $_POST['Location'] ,
            "Starts" => $_POST['Starts'] ,
            "Ends" => $_POST['Ends'] ,
            "Image" => $_POST['Image'] ,
            "Description" => $_POST['Description'] ,
            "OrganiserName" => $_POST['OrganiserName'] ,
            "OrganiserDescription" => $_POST['OrganiserDescription'] ,

            "EventCategoryId" => $_POST['EventCategoryId'] ,
            "EventTopicId" => $_POST['EventTopicId'] ,

        );

        if (\AnOrmApart\Dal::create('Event', $Event, 'Name')) {
            $model['message'] = "Rij toegevoegd! {$Event['Name']} is toegevoegd aan Event";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$Event['Name']} niet toevoegen aan Event";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        return $this->view($model, 'Views/Event/Index.php');
    }
    public function readingOne($Id)
    {
        $model['row'] = \AnOrmApart\Dal::readOne('Event', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        return $this->view($model);
    } 

    public function deleteOne($Id) 
    { 
        $EventOne= \AnOrmApart\Dal::readOne('Event', $Id);

        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();


        if (\AnOrmApart\Dal::delete('Event',$Id)) {
            $model['message'] = "Event met de naam {$EventOne['Name']} is verwijderd aan Event Table";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan niet {$EventOne['Name']} verwijderd zijn van Event table";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        return $this->view($model, 'Views/Event/DeleteOne.php');

        
    }   
    
    
    public function UpdatingOne($Id) 
    { 
        $model['row'] = \AnOrmApart\Dal::readOne('Event', $Id);
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();
        
        return $this->view($model, 'Views/Event/UpdatingOne.php');

        
    }
    public function UpdateOne()
    {
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        $model['message'] = \AnOrmApart\Dal::getMessage();

        $event = array(

            "Id" => $_POST['Id'] ,
            "Name" => $_POST['Name'] ,
            "Location"=> $_POST['Location'] ,
            "Starts" => $_POST['Starts'] ,
            "Ends" => $_POST['Ends'] ,
            "Image" => $_POST['Image'] ,
            "Description" => $_POST['Description'] ,
            "OrganiserName" => $_POST['OrganiserName'] ,
            "OrganiserDescription" => $_POST['OrganiserDescription'] ,

            "EventCategoryId" => $_POST['EventCategoryId'] ,
            "EventTopicId" => $_POST['EventTopicId'] , 

        );

        if (\AnOrmApart\Dal::update('Event', $event, 'Name')) {
            $model['message'] = "Update is geworden voor {$event['Name']} ";
        } else {
            $model['message'] = "Oeps er is iets fout gelopen! Kan {$event['Name']} niet update ";
            $model['error'] = \AnOrmApart\Dal::getMessage();
        }
        $model['list'] = \AnOrmApart\Dal::readAll('Event');
        return $this->view($model, 'Views/Event/Index.php');
    }
  
}
