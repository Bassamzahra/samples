<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fric-frac simple CRUD</title>
    <link rel="stylesheet" type="text/css" href="css/icon-font.css">
    <link rel="stylesheet" type="text/css" href="css/app.css">
</head>


<body class="page">


<article class="index">
    <a class="tile" href="/EventCategory/Index">
        <span aria-hidden="true" class="icon-bookmark"></span>
        <span class="screen-reader-text">Event Category</span>
        Event Category
    </a>
    <a class="tile" href="/EventTopic/Index">
    <span aria-hidden="true" class="icon-price-tag"></span>
        <span class="screen-reader-text">Event Topic</span>
        Event Topic</a>
    <a class="tile" href="/Event/Index">
    <span aria-hidden="true" class="icon-power"></span>
        <span class="screen-reader-text">Event</span>
    Event</a>
    
    <div class="tile _2x1">Informatieve tegel</div>
</article>

   
</body>

</html>