<section class="show-room entity">
    <form id="form" method="" action="" class="detail">
        <nav class="command-panel">
            <h2 class="banner">Event</h2>
            
            <a href="/Event/Index" class="tile">
                <span class="icon-cross"></span>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>
        <fieldset>
        <div class="feedback">
            <p><?php echo $model['message']; ?></p>
            <p><?php echo isset($model['error']) ? $model['error'] : ''; ?></p>
        </div>
        </fieldset>
        <div class="feedback"></div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>