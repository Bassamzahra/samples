<section class="show-room entity">
    <form id="form" method="" action="" class="detail">
        <nav class="command-panel">
            <h2 class="banner">Event</h2>
            <a href="/Event/UpdatingOne/<?php echo $model['row']['Id'];?>" class="tile">
                <span class="icon-pencil"></span>
                <span class="screen-reader-text">Updating One</span>
            </a>
            <a href="/Event/InsertingOne" class="tile">
                <span class="icon-plus"></span>
                <span class="screen-reader-text">Inserting One</span>
            </a>
            <a href="/Event/deleteOne/<?php echo $model['row']['Id'];?>" class="tile">
                <span class="icon-bin"></span>
                <span class="screen-reader-text">Delete One</span>
            </a>
            <a href="/Event/Index" class="tile">
                <span class="icon-cross"></span>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>  
        <fieldset>
            <div>
                <label for="Name">Naam :</label>
                <span><?php echo $model['row']['Name']; ?></span>
                </div>
                <div>
                <label for="Location">Location :</label>
                <span> <?php echo $model['row']['Location']; ?> </span>
            </div>
            <div>
                <label for="Starts">Starts :</label>
                <span><?php echo $model['row']['Starts']; ?></span>
                </div>
                <div>
                <label for="Ends">Ends :</label>
                <span><?php echo $model['row']['Ends']; ?></span>
            </div>
            <div>
                <label for="Image">Image :</label>
                <span><?php echo $model['row']['Image']; ?></span>
                </div>
                <div>
                <label for="Descriptionm">Descriptionm :</label>
                <span><?php echo $model['row']['Description']; ?></span>
            </div>
            <div>
                <label for="OrganiserName">OrganiserName :</label>
                <span><?php echo $model['row']['OrganiserName']; ?></span>
            </div>
            <div>
                <label for="OrganiserDescription">OrganiserDescription :</label>
                <span><?php echo $model['row']['OrganiserDescription']; ?></span>
            </div>
            <div>
                <label for="EventCategoryId">EventCategoryId :</label>
                <span><?php echo $model['row']['EventCategoryId']; ?></span>
            </div>
            <div>
                <label for="EventTopicId">EventTopicId :</label>
                <span><?php echo $model['row']['EventTopicId']; ?></span>
            </div>
            
        </fieldset>
        <div class="feedback"></div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>