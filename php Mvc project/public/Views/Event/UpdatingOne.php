<section class="show-room entity">
    <form id="form" method="post" action="/Event/UpdateOne" class="detail">
        <nav class="command-panel">
            <h2 class="banner">Event</h2>
            <button type="submit" value="insert" name="uc" class='tile'>
                <span class="icon-floppy-disk"></span>
                <span class="screen-reader-text">Update One</span>
            </button>
            <a href="/Event/Index" class="tile">
                <span class="icon-cross"></span>
                <span class="screen-reader-text">Annuleren</span>
            </a>
        </nav>
        <fieldset>
        <div>
                <label for="Id">Id</label> 
                <input type="text" required id="Id" name="Id" value= <?php echo $model['row']['Id']; ?> readonly/>
            </div>
            <div>
                <label for="Name">Naam</label> 
                <input type="text" required id="Name" name="Name" value= <?php echo $model['row']['Name']; ?> require/>
            </div>
            <div>
                <label for="Location">Locatie</label>
                <input type="text" required id="Location" name="Location" value="<?php echo $model['row']['Location'];?>"  />
            </div>
            <div>
                <label for="Starts">Start</label>
                <input id="Starts" name="Starts" type="datetime-local" 
                    value="<?php echo $model['row']['Starts'];?>" />
            </div>
            <div>
                <label for="Ends">Einde</label>
                <input id="Ends" name="Ends" type="datetime-local" 
                    value="<?php echo $model['row']['Ends'];?>" />
            </div>
            <div>
                <label for="Image">Afbeelding</label>
                <input id="Image" name="Image" type="text" 
                    value="<?php echo $model['row']['Image'];?>" required />
            </div>
            <div>
                <label for="Description">Beschrijving</label>
                <input id="Description" name="Description" type="text" 
                    value="<?php echo $model['row']['Description'];?>" required />
            </div>
            <div>
                <label for="OrganiserName">Organizator naam</label>
                <input id="OrganiserName" name="OrganiserName" type="text" 
                    value="<?php echo $model['row']['OrganiserName'];?>" required />
            </div>
            <div>
                <label for="OrganiserDescription">Organizator beschrijving</label>
                <input id="OrganiserDescription" name="OrganiserDescription" type="text" 
                    value="<?php echo $model['row']['OrganiserDescription'];?>" required />
            </div>



            <div>

<?php
                  $eventCatagoryList['list'] = \AnOrmApart\Dal::readAll('EventCategory');
                  $eventTopicList['list'] = \AnOrmApart\Dal::readAll('EventTopic');

                   ?> 

               <label for="EventCategoryId">Event categorie</label>
               <select id="EventCategoryId" name="EventCategoryId">
                   <!-- option elementen --> 
                   <option  > <?php echo $model['row']['EventCategoryId'] ?>    </option>
                      <?php
               foreach ($eventCatagoryList['list'] as $eventcategory) {
               ?>
           <option value="<?php echo $eventcategory['Id'] ?> " >  <?php echo $eventcategory['Name'] ?>    </option>
               <?php } ?> 
               </select>                
               </div> 

             <div>
               <label for="EventTopicId">Event topic</label>
               <select id="EventTopicId" name="EventTopicId" >
                   <!-- option elementen -->
                     <!-- option elementen --> 
                     <option  > <?php echo $model['row']['EventTopicId'] ?>  </option>
                      <?php
               foreach ($eventTopicList['list'] as $eventTopicList) {
               ?>
           <option value="<?php echo $eventTopicList['Id'] ?> " >  <?php echo $eventTopicList['Name'] ?>    </option>
               <?php } ?> 
               </select>
           </div>
 
            
        </fieldset>
        <div class="feedback">
            <p><?php echo $model['message']; ?></p>
            <p><?php echo isset($model['error']) ? $model['error'] : ''; ?></p>
        </div>
    </form>
    <?php include('ReadingAll.php'); ?>
</section>