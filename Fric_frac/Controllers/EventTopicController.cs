﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

namespace FricFrac.Controllers
{
    public class EventTopicController : Controller
    {

        private docent1Context dbContext;
        public EventTopicController(docent1Context dbContext)
        {
            this.dbContext = dbContext;
        }
        public IActionResult Index()
        {


            ViewBag.Title = "Fric-frac EventTopic Index";
            return View(dbContext.EventTopic.ToList());
        }

        public IActionResult InsertingOne()
        {

            ViewBag.Title = "Fric-frac EventTopic Inserting One";
            return View(dbContext.EventTopic.ToList());

        }

        public IActionResult InsertOne(Models.FricFrac.EventTopic EventTopic)
        {

            ViewBag.Message = "Insert een land in de database";
            dbContext.EventTopic.Add(EventTopic);
            dbContext.SaveChanges();
            return View("Index", dbContext.EventTopic);

        }

        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
            {
                return NotFound();
            }

            var EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == id);
            if (EventTopic == null)
            {
                return NotFound();
            }
            return View(EventTopic);
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac EventTopic Updating One";
            if (id == null)
            {
                return NotFound();
            }

            var EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == id);
            if (EventTopic == null)
            {
                return NotFound();
            }
            return View(EventTopic);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.EventTopic EventTopic)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(EventTopic);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.EventTopic.Any(e => e.Id == EventTopic.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", EventTopic);
        }

        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            // zoek het land dat verwijderd moet worden
            var EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == id);
            if (EventTopic == null)
            {
                return NotFound();
            }
            dbContext.EventTopic.Remove(EventTopic);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
        public IActionResult Cancel()
        {


            return RedirectToAction("Index");
        }











    }
}