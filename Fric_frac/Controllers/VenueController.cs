﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

namespace FricFrac.Controllers
{
    public class VenueController : Controller
    {
        private docent1Context dbContext;

        public VenueController(docent1Context dbContext)
        {
            this.dbContext = dbContext;
        }

        public IActionResult Index()
        {

            ViewBag.Title = "Fric-frac Venue Index";
            return View(dbContext.Venue.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Venue Inserting One";
            ViewBag.Venues = dbContext.Venue.ToList();
            return View();



        }


        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.Venue venue)
        {
            ViewBag.Message = "Insert een Venue in de database";
            dbContext.Venue.Add(venue);
            dbContext.SaveChanges();
            return View("Index", dbContext.Venue);
        }
     

     

        [HttpGet]
        public IActionResult ReadingOne(string? name)
        {
            ViewBag.Message = "Lees een Venue in de database";
            if (name == null)
            {
                return NotFound();
            }

            var venue = dbContext.Venue.SingleOrDefault(m => m.Name == name);
            if (venue == null)
            {
                return NotFound();
            }
            venue = dbContext.Venue.SingleOrDefault(m => m.Name == venue.Name);
            return View(venue);
        }

    }
}