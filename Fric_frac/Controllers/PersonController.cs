﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FricFrac.Models.FricFrac;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FricFrac.Controllers
{
    public class PersonController : Controller


    {

        private docent1Context dbContext;

        public PersonController(docent1Context dbContext)
        {
            this.dbContext = dbContext;
        }



        
        // GET: /<controller>/
        public IActionResult Index()
        {

            ViewBag.Title = "Fric-frac Person Index";
            return View(dbContext.Person.ToList());
        }

      
        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Person Inserting One";
            ViewBag.Countries = dbContext.Country.ToList();
            return View();


        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.Person person)
        {
            ViewBag.Message = "Insert een persoon in de database";
            dbContext.Person.Add(person);
            dbContext.SaveChanges();
            return View("Index", dbContext.Person);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Lees een Persoon in de database";
            if (id == null)
            {
                return NotFound();
            }

            var person = dbContext.Person.SingleOrDefault(m => m.Id == id);
            if (person == null)
            {
                return NotFound();
            }
            person.Country = dbContext.Country.SingleOrDefault(m => m.Id == person.CountryId);
            return View(person);
        }
        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.Person person)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(person);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.Person.Any(e => e.Id == person.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", dbContext.Person);
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Person UpdatingOne";
            if (id == null)
            {
                return NotFound();
            }
            var person = dbContext.Person.SingleOrDefault(m => m.Id == id);
            if (person == null)
            {
                return NotFound();
            }
            person.Country = dbContext.Country.SingleOrDefault(m => m.Id == person.CountryId);
            ViewBag.Countries = dbContext.Country.ToList();
            return View(person);
        }
        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }
        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var person = dbContext.Person.SingleOrDefault(m => m.Id == id);
            if (person == null)
            {
                return NotFound();
            }
            dbContext.Person.Remove(person);
            dbContext.SaveChanges();
           
            return RedirectToAction("Index");
        }


    }
}
