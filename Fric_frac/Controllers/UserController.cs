﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;


namespace FricFrac.Controllers
{
    public class UserController : Controller
    {

        private docent1Context dbContext;
        public UserController(docent1Context dbContext)
        {
            this.dbContext = dbContext;
        }
        public IActionResult Index()
        {


            ViewBag.Title = "Fric-frac User Index";

            return View(dbContext.User.ToList());

        }

        public IActionResult InsertingOne()
        {

            
            ViewBag.Title = "Fric-frac User Inserting One";
            ViewBag.Roles = dbContext.Role.ToList();
            ViewBag.Persons = dbContext.Person.ToList();
            return View();

        }

        public IActionResult InsertOne(Models.FricFrac.User User)
        {

            ViewBag.Message = "Insert een land in de database";
            dbContext.User.Add(User);
            dbContext.SaveChanges();
            return View("Index", dbContext.User);

        }

      
        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Lees een User in de database";
            if (id == null)
            {
                return NotFound();
            }

            var user = dbContext.User.SingleOrDefault(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            user.Role = dbContext.Role.SingleOrDefault(m => m.Id == user.RoleId);
            user.Person = dbContext.Person.SingleOrDefault(m => m.Id == user.PersonId);


            return View(user);
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {

            ViewBag.Title = "Fric-frac User Updating One";
            if (id == null)
            {
                return NotFound();
            }
            var user = dbContext.User.SingleOrDefault(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            user.Role = dbContext.Role.SingleOrDefault(m => m.Id == user.RoleId);
            user.Person = dbContext.Person.SingleOrDefault(m => m.Id == user.PersonId);
            ViewBag.Roles = dbContext.Role.ToList();
            ViewBag.Persons = dbContext.Person.ToList();

            return View(user);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.User User)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(User);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.User.Any(e => e.Id == User.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", User);
        }

        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            // zoek het land dat verwijderd moet worden
            var User = dbContext.User.SingleOrDefault(m => m.Id == id);
            if (User == null)
            {
                return NotFound();
            }
            dbContext.User.Remove(User);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
        public IActionResult Cancel()
        {


            return RedirectToAction("Index");
        }











    }
}