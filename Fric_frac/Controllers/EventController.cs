﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

namespace FricFrac.Controllers
{
    public class EventController : Controller
    {

        private docent1Context dbContext;
        public EventController(docent1Context dbContext)
        {
            this.dbContext = dbContext;
        }
        public IActionResult Index()
        {


            ViewBag.Title = "Fric-frac Event Index";

            return View(dbContext.Event.ToList());

        }

        public IActionResult InsertingOne()
        {


            ViewBag.Title = "Fric-frac Event Inserting One";
            ViewBag.EventTopics = dbContext.EventTopic.ToList();
            ViewBag.EventCategorys = dbContext.EventCategory.ToList();
            return View();

        }

        public IActionResult InsertOne(Models.FricFrac.Event Event)
        {

            ViewBag.Message = "Insert een land in de database";
            dbContext.Event.Add(Event);
            dbContext.SaveChanges();
            return View("Index", dbContext.Event);

        }


        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Lees een Event in de database";
            if (id == null)
            {
                return NotFound();
            }

            var event1 = dbContext.Event.SingleOrDefault(m => m.Id == id);

            if (event1 == null)
            {
                return NotFound();
            }

            event1.EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == event1.EventTopicId);
            event1.EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == event1.EventCategoryId);
            ViewBag.EventTopics = dbContext.EventTopic.ToList();
            ViewBag.EventCategorys = dbContext.EventCategory.ToList();

            return View(event1);
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {

            ViewBag.Title = "Fric-frac Event Updating One";
            if (id == null)
            {
                return NotFound();
            }
           var event1 = dbContext.Event.SingleOrDefault(m => m.Id == id);

            if (event1 == null)
            { 
                return NotFound();
            }

            event1.EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == event1.EventTopicId);
            event1.EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == event1.EventCategoryId);
            ViewBag.EventTopics = dbContext.EventTopic.ToList();
            ViewBag.EventCategorys = dbContext.EventCategory.ToList();

            return View(event1);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.Event Event)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(Event);
                    dbContext.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.Event.Any(e => e.Id == Event.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return View("Index", dbContext.Event);
        }

        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            // zoek het land dat verwijderd moet worden
            var Event = dbContext.Event.SingleOrDefault(m => m.Id == id);
            if (Event == null)
            {
                return NotFound();
            }
            dbContext.Event.Remove(Event);
            dbContext.SaveChanges();
            // keer terug naar de index pagina
            return RedirectToAction("Index");
        }
        public IActionResult Cancel()
        {


            return RedirectToAction("Index");
        }











    }
}