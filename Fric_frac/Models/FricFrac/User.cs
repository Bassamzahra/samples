﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    public partial class User
    {

        public User ()
        
        { 
        
        }  


        [Required]
        [FromForm(Name = "User-Id")]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        [FromForm(Name = "User-Name")]
        public string Name { get; set; }

        [StringLength(255)]
        [FromForm(Name = "User-Salt")]
        public string Salt { get; set; }


        [StringLength(255)]
        public string HashedPassword { get; set; }



        [Column(TypeName = "int(11)")]
        [FromForm(Name = "User-PersonId")]

        public int? PersonId { get; set; }


        [Column(TypeName = "int(11)")]
        [FromForm(Name = "User-RoleId")]
        public int? RoleId { get; set; }



        [ForeignKey("PersonId")]
        public Person Person { get; set; }

        [ForeignKey("RoleId")]
        public Role Role { get; set; }










    }
}
