﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    public partial class EventCategory
    {


        public EventCategory()
        {
          
        }

        [Required]
        [StringLength(150)]
        [FromForm(Name = "Eventcategory-Name")]
        public string Name { get; set; }


        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Eventcategory-Id")]
        public int Id { get; set; }


       
    }
}
