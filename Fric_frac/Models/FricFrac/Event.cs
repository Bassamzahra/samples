﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    public partial class Event
    {

        public Event () { }

        [Required]
        [FromForm(Name = "Event-Name")]
        [StringLength(120)]
        public string Name { get; set; }

        [FromForm(Name = "Event-Location")]
        [StringLength(120)]
        public string Location { get; set; }

        [FromForm(Name = "Event-Starts")]
        [Column(TypeName = "datetime")]
        public DateTime? Starts { get; set; }

        [FromForm(Name = "Event-Ends")]
        [Column(TypeName = "datetime")]
        public DateTime? Ends { get; set; }

        [FromForm(Name = "Event-Image")]
        [StringLength(255)]
        public string Image { get; set; }

        [FromForm(Name = "Event-Description")]
        [StringLength(1024)]
        public string Description { get; set; }

        [FromForm(Name = "Event-OrganiserName")]
        [StringLength(120)]
        public string OrganiserName { get; set; }

        [FromForm(Name = "Event-Description")]
        [StringLength(120)]
        public string OrganiserDescription { get; set; }

        [Required]
        [FromForm(Name = "Event-EventCategoryId")]
        [Column(TypeName = "int(11)")]
        public int? EventCategoryId { get; set; }

        [Required]
        [FromForm(Name = "Event-EventTopicId")]
        [Column(TypeName = "int(11)")]
        public int? EventTopicId { get; set; }

        [Required]
        [FromForm(Name = "Event-Id")]
        [Column(TypeName = "int(11)")]
        public int Id { get; set; }



        [ForeignKey("EventCategoryId")]
        public  EventCategory EventCategory { get; set; }

        [ForeignKey("EventTopicId")]
        public  EventTopic EventTopic { get; set; }
    }
}
