﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace FricFrac.Models.FricFrac
{
    public class Venue
    {

        public Venue ()
        { }

        [Required]
        [Key]
        [StringLength(50)]
        [FromForm(Name = "Venue-Name")]
        public string Name { get; set; }

        [ForeignKey("CountryId")]
        public Country Country { get; set; }


    }
}
