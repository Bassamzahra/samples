﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AntwerpenHotels.Models
{
    public class AhLike
    {

        public int Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; } 
        public int Likes { get; set; } 
        public AhLike() { }
        public AhLike(string key)
        {
            this.Id = 0;
            this.Key = key;
            this.Name = "niet van toepassing";
            this.Likes = 0;
        }

    }
}
