﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AntwerpenHotels.Models;
using Microsoft.EntityFrameworkCore;

namespace AntwerpenHotels.Models
{ 
    public class AhContext : DbContext 
      {
       
        public AhContext(DbContextOptions<AhContext> options) : base(options)
        {




        }
        public  DbSet<AhLike> AhLikes { get; set; }
        public DbSet<AhComment> AhComments { get; set; }
    }
}
