﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AntwerpenHotels.Models;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;


namespace AntwerpenHotels.Controllers
  
{
    [ApiController]
    [Route("[controller]")]

    public class AhLikeController : ControllerBase
    {

        private readonly AhContext ahContext;
        public AhLikeController(AhContext context)
        {
            ahContext = context;
        }


        [HttpGet("{key}")]
        public AhLike GetLikes(string key)
        {
            var ahLike = ahContext.AhLikes.Where(a => a.Key == key).FirstOrDefault();

            ahLike = ahLike == null ? new AhLike(key) : ahLike;
            return ahLike;

        }

        [HttpPost]
        public AhLike PostLike(AhLike item)
        {
            var ahLike = ahContext.AhLikes.Where(a => a.Key == item.Key).FirstOrDefault();

            if (ahLike == null)
            {
                ahContext.AhLikes.Add(item);
                ahContext.SaveChanges();
                ahLike = item;
            }
            else
            {
                ahLike.Likes = ahLike.Likes + 1;
                ahContext.AhLikes.Update(ahLike);
                ahContext.SaveChanges();
            }
            return ahLike;
        }



    }
    
}
