﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AntwerpenHotels.Models;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;

namespace AntwerpenHotels.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AhCommentController : ControllerBase
    {

        private readonly AhContext ahContext;
        public AhCommentController(AhContext context)
        {
            ahContext = context;
        }


        [HttpGet("{key}")]
        public AhComment GetComments(string key)
        {
            var ahComment = ahContext.AhComments.Where(a => a.Key == key).FirstOrDefault();

            ahComment = ahComment == null ? new AhComment(key) : ahComment;
            return ahComment;

        }

        [HttpPost]
        public AhComment PostComment(AhComment item)
        {
            AhComment ahComment = item;

          
                ahContext.AhComments.Add(item);
                ahContext.SaveChanges();
               
         
            return ahComment;
        }


    }
}